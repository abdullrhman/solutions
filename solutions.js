
export default function f1(arr1,arr2){
	for (let i=0 ;i< arr1.length ;i++) {
	   if(arr2.includes(arr1[i])){
			let index = arr2.indexOf(arr1[i]);
				arr1.splice(i,1);
				arr2.splice(index,1);
	   }
	}
 
	for (let i=0 ;i< arr1.length ;i++) {
	   if(arr1.includes(arr2[i])){
			let index = arr1.indexOf(arr2[i]);
				arr2.splice(i,1);
				arr1.splice(index,1);
	   }
	}

	return arr1.concat(arr2).length;
}

export default function f2(arr1,arr2){
	var temp=[];
	for (let i=0 ;i< arr2.length ;i++) {
   		temp[i] =0;
   		arr1.forEach(item =>{
        if(arr2[i] == item){
          temp[i] += 1 ;
        }
      });
    }
	return temp;
}

export default function f3(str){
	var pattern = new RegExp("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$")
	if(!pattern.test(str)){
		return 0;
	}else{
		//str
		let temp = str.split(".");
		return temp.length;
	}
}

